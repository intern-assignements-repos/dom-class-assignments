let inputHeaderBox = document.getElementById("input-header");
let inputDescriptionBox = document.getElementById("input-todo-description");
let selectedStatus = document.getElementById("select-todo-status");
let addBtn = document.querySelector("#add-btn");
let ul = document.querySelector("ul");

function addTodo(data) {
  if (data.length !== 0) {
    ul.innerHTML += `
        <li class="todo-item">
            <div class="todo-item-header">
              <h3>${data.title}</h3>
              <div class="input-todo-status">
                <select name="todo-status" id="select-todo-status" >                
                  <option value="1" ${
                    data.status === 1 ? "selected" : ""
                  }>Pending</option>
                  <option value="2" ${
                    data.status === 2 ? "selected" : ""
                  } >On Progress</option>
                  <option value="3" ${
                    data.status === 3 ? "selected" : ""
                  } >Completed</option>
                </select>
              </div>
            </div>
            <div class="todo-item-body">
              <p>
                ${data.description}
              </p>
              <button class="todo-delete" id="delete-btn" onclick="removeTodo(event)">
                <i class="fa-solid fa-trash"></i>
              </button>
            </div>
          </li>
        `;
  } else {
    alert("Need to enter some value!");
  }
}

function todoValidation(e) {
    let pError = document.getElementById("errorMsg");
    pError.innerText = ""
    pError.setAttribute("display", "none")
    if (inputHeaderBox.value.toString().trim().length !== 0) {
        const data = {
            title: inputHeaderBox.value.toString().trim(),
            description: inputDescriptionBox.value.toString().trim(),
            status: parseInt(selectedStatus.value),
        };        
        addTodo(data);
    } else {        
        pError.setAttribute("style", "color: red");
        inputHeaderBox.addEventListener('invalid', inputHeaderBox)
        inputHeaderBox.setCustomValidity('Enter a task Heading')
        pError.innerText = inputHeaderBox.validationMessage;        
        pError.setAttribute("display", "flex")        
        setTimeout(()=> {
            pError.innerText ="";
        }, 1000)
        inputHeaderBox.value = "";
  }

  return false;
}

function removeTodo(e) {
  let item = e.target.parentElement.parentElement.parentElement;
  if (confirm("Are You sure?")) {
    item.remove();
  }
}