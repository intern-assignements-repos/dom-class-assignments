const myFormInputs = [
  {
    name: "firstName",
    label: "First Name *",
    type: "text",
    placeholder: "Enter your First Name . . .",
    errorMessage: "Please Enter a Valid First Name",
    required: true,
    errored: false
  },
  {
    name: "lastName",
    label: "Last Name *",
    type: "text",
    placeholder: "Enter your Last Name . . .",
    errorMessage: "Please Enter a Valid Last Name",
    required: true,
    errored: false
  },
  {
    name: "email",
    label: "Email *",
    type: "email",
    placeholder: "Enter your Email . . .",
    errorMessage: "Please Enter a Valid Email",
    required: true,
    errored: false
  },
  {
    name: "address",
    label: "Address",
    type: "text",
    placeholder: "Enter your Address . . .",
    errorMessage: "Please Enter a Valid Address",
    required: false,
    errored: false
  }
];

const renderFormElements = (inputFields) => {
  let inputList = document.getElementById("input-list");
  let listInnerHTML = ``;
  inputFields.map((item) => {
    listInnerHTML += `
        <li>
            <label for="${item.name}">${item.label}</label>
            <input 
                type="${item.type}" 
                name="${item.name}" 
                id="${item.name}" 
                ${item.required ? "required" : ""}
            >            
            <p name="errorMessage" class="errorMessage">${item.errorMessage}</p>
        </li>
    `;
  });
  inputList.innerHTML = listInnerHTML;
};

renderFormElements(myFormInputs);

const showError = (input) => {
    let parent = input.parentElement;
    let p = parent.querySelector("p");
    p.style.display = "flex";
    setTimeout(() => p.style.display = "none", 3000);
}

const checkValidation = (e) => {
    let flag = true;
    e.preventDefault();
    let firstName = document.getElementsByName("firstName")[0];
    let lastName = document.getElementsByName("lastName")[0];
    let email = document.getElementsByName("email")[0];    
  
    if(firstName.value.trim().length === 0){
        showError(firstName)
        flag = false;
    }
    if(lastName.value.trim().length === 0){
        showError(lastName)
        flag = false;
    }
    if(email.value.trim().length === 0){
        showError(email)
        flag = false;
    }

    flag ? alert("Success") : null;
    return flag;
};
