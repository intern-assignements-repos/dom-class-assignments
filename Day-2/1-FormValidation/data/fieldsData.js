export const myFormInputs = [
    {
        name: "firstName",
        type: "text",
        placeholder: "Enter your First Name . . .",
        required: true
    },
    {
        name: "lastName",
        type: "text",
        placeholder: "Enter your Last Name . . .",
        required: true
    },
    {
        name: "email",
        type: "text",
        placeholder: "Enter your Email . . .",
        required: true
    },
    {
        name: "address",
        type: "text",
        placeholder: "Enter your Address . . .",
        required: false
    }    
];