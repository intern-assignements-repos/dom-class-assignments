const myRoutine = [
    "Go to the Gym",
    "Get Ready",
    "Eat Breakfast"
]

let inputBox = document.getElementById("input-box");
let addBtn = document.querySelector("#add-btn");
let ul = document.querySelector("ul");
let liSpan = document.getElementsByTagName("span");

function renderElement() {
    myRoutine.forEach(item => {          
        ul.innerHTML += `
        <li>            
            <h4>${item}</h4>
            <button onclick="removeElement(this.parentElement)">
                <i class="fa-solid fa-xmark fa-xl"></i>
            </button>
        </li>
        `;
    })
}

renderElement();

function addSubject(subject) {
    if(subject !== ""){        
        ul.innerHTML += `
        <li>            
            <h4>${subject}</h4>
            <button onclick="removeElement(this.parentElement)">
                <i class="fa-solid fa-xmark fa-xl"></i>
            </button>
        </li>
        `
    }else{
        alert("Subject Name Can't be empty!")
    }
}

addBtn.addEventListener('click', () => {
    const val = inputBox.value;
    addSubject(val);
});


function removeElement(parent) {
    if(confirm("Are You sure?")){
        parent.remove();
    }        
}

