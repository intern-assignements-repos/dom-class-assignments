const keyValue = {
  1: "checkbox",
  2: "color",
  3: "date",
  4: "datetime-local",
  5: "email",
  6: "file",
  7: "hidden",
  8: "image",
  9: "month",
  10: "number",
  11: "password",
  12: "radio",
  13: "range",
  14: "search",
  15: "tel",
  16: "text",
  17: "time",
  18: "url",
  19: "week",
};

let formSelector = document.getElementsByName("form-select")[0];
let inputLabel = document.getElementById("input-label");
let inputRequired = document.getElementById("required-input");
let inputList = document.getElementById("input-list");

function createInput() {
  inputList.innerHTML += `
    <li id="list-item">
        <input 
            type="${keyValue[parseInt(formSelector.value)]}" 
            placeholder="${inputLabel.value}" 
            ${inputRequired.checked ? "required" : ""}/>
            <button id="removeBtn" onclick="removeItem(event)">✖</button>
    </li>
  `;

  return false;
}


function formSubmission() {
    return false;
}

function removeItem(e) {
    let parent = e.target.parentElement;
    parent.remove();
}